alias lx='ls -X'
alias temp='tlp stat -t'
alias ct='clear && task next'

alias pbcopy='xclip -selection clipboard'
alias pbpaste='xclip -selection clipboard -o'
