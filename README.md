# dotfiles

Dotfiles from my 2018 Ubuntu install.

Contains:

- .pinerc (personal info removed)
- .bashrc
- .bash_aliases
- .zshrc (I use ohmyzsh)
- .taskrc (TaskWarrior)
- newsboat config
- .nanorc
- tint2rc
- Atom config.cson and package list
- .vimrc
